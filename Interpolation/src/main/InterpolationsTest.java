package main;


import java.util.Random;

public class InterpolationsTest {

	final static int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Interpolation is = new Interpolation();
		long[] zahlenliste = new long[20000000];
		zahlenliste = is.getSortedList(zahlenliste);

		Random rand = new Random(111111);
		long gesuchteZahl = 0;

		long time1 = System.currentTimeMillis();
		gesuchteZahl = zahlenliste[1];
		System.out.println("Best Case Lineare Suche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.lineareSuche(zahlenliste, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time1 + "ms");
		System.out.println("");

		long time2 = System.currentTimeMillis();
		gesuchteZahl = zahlenliste[zahlenliste.length - 10000000];
		System.out.println("Average Case Lineare Suche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.lineareSuche(zahlenliste, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time2 + "ms");
		System.out.println("");

		long time3 = System.currentTimeMillis();
		gesuchteZahl = zahlenliste[zahlenliste.length - 1];
		System.out.println("Worst Case Lineare Suche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.lineareSuche(zahlenliste, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time3 + "ms");
		System.out.println("");

		long time4 = System.currentTimeMillis();
		gesuchteZahl = zahlenliste[1];
		System.out.println("Best Case Interpolationssuche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.suche(zahlenliste, 1, zahlenliste.length - 1, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time4 + "ms");
		System.out.println("");

		long time5 = System.currentTimeMillis();
		gesuchteZahl = rand.nextInt(10000);
		System.out.println("Average Case Interpolationssuche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.suche(zahlenliste, 1, zahlenliste.length - 1, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time5 + "ms");
		System.out.println("");

		long time6 = System.currentTimeMillis();
		gesuchteZahl = zahlenliste[zahlenliste.length - 1];
		System.out.println("Worst Case Interpolationssuche");
		System.out.println("Gesuchte Zahl: " + gesuchteZahl);
		System.out.println(is.suche(zahlenliste, 1, zahlenliste.length - 1, gesuchteZahl));
		System.out.println(System.currentTimeMillis() - time6 + "ms");
		System.out.println("");
		// for(long x: a)
		// System.out.println(x);

	}
}
