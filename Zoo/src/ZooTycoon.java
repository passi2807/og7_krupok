
public class ZooTycoon {
	
	private String name;
	private double preis;
	private int id;
	private int verfuegbarkeit;
	private short maxKaufzahl;
	private double addonWert;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVerfuegbarkeit() {
		return verfuegbarkeit;
	}
	public void setVerfuegbarkeit(int verfuegbarkeit) {
		this.verfuegbarkeit = verfuegbarkeit;
	}
	public short getMaxKaufzahl() {
		return maxKaufzahl;
	}
	public void setMaxKaufzahl(short maxKaufzahl) {
		this.maxKaufzahl = maxKaufzahl;
	}
	public double getAddonWert() {
		return addonWert;
	}
	public void setAddonWert(double addonWert) {
		this.addonWert = addonWert;
	
	}
	@Override
	public String toString() {
		return "ZooTycoon [name=" + name + ", preis=" + preis + ", id=" + id + ", verfuegbarkeit=" + verfuegbarkeit
				+ ", maxKaufzahl=" + maxKaufzahl + ", addonWert=" + addonWert + "]";
	}
}
	



