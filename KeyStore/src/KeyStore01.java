

public class KeyStore01 extends java.lang.Object {

	String[] var;

	public KeyStore01() {
		this(100);
	}

	public KeyStore01(int length) {
		this.var = new String[length];
	}

	public boolean add(String e) {
		for (int i = 0; i < var.length-1; i++) {
			if (var[i] == null) {
				var[i] = e;
				return true;
			}
		}
		return false;
	}

	public String toString() {
		String ausgabe = "";
		for (int i = 0; i < var.length -1; i++) {
			if (var[i] != null)
				ausgabe = ausgabe + "   Key=  " + var[i] + "|";
		}
		return ausgabe;

	}

	public String get(int index) {
		return var[index];
	}

	public int size() {
		int size = 0;
		for(int i = 0; i < var.length; i ++) {
			if(var[i] != null){
				size++;
				
			}
		}
		return size;
	}

	public int indexOf(String str) {
		for (int i = 0; i < var.length -1; i++) {
			if (str.equals(var[i]))
				return i;
		}
		return -1;

	}

	public boolean remove(int index) {
		if (index < var.length) {
			var[index] = null;
			return true;
		}
		return false;

	}

	public boolean remove(String str) {
		for (int i = 0; i < var.length -1; i++) {
			if (var[i].equals(str)) {
				var[i] = null;
				return true;
			}
		}
		return false;
	}

}
