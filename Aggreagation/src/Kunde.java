
public class Kunde extends Benutzer {

	private int registrierDatum;

	public Kunde(int benutzerID, String passwort, String name, String email, int registrierDatum) {
		super(benutzerID, passwort, name, email);
		this.registrierDatum = registrierDatum;
	}

	public int getRegistrierDatum() {
		return registrierDatum;
	}

	public void setRegistrierDatum(int registrierDatum) {
		this.registrierDatum = registrierDatum;
	}
	
	
}
