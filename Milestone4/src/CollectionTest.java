
import java.util.*;
import java.util.Collections;

public class CollectionTest {

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Autor bitte eingeben: ");
				String autor = myScanner.next();
				System.out.println("Name eingeben:");
				String titel = myScanner.next();
				System.out.println("ISBN eingeben: ");
				String isbn = myScanner.next();
				CollectionTest.add(buchliste, autor, titel, isbn);
				break;

			case '2':
				System.out.println("\nWelches Buch soll gefunden werden? Bitte ISBN angeben");
				eintrag = myScanner.next();
				CollectionTest.findeBuch(buchliste, eintrag);
				System.out.println(CollectionTest.findeBuch(buchliste, eintrag));
				break;
			case '3':
				System.err.println("\nWelches Buch soll gel�scht werden? Bitte ISBN angeben");
				eintrag = myScanner.next();
				CollectionTest.loesche(buchliste, eintrag);
				System.out.println("Buch wurde gel�scht");
				break;
			case '4':
				System.out.println("\nDie gr��te ISBN:");
				System.out.println(CollectionTest.groesste(buchliste));

				break;
			case '5':
				System.out.println("\nListe: ");
				System.out.println(CollectionTest.zeige(buchliste));
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static void add(List<Buch> buchliste, String autor, String titel, String isbn) {
		Buch b1 = new Buch(autor, titel, isbn);
		buchliste.add(b1);

	}

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).getIsbn().equals(isbn)) {
				return buchliste.get(i);
			}
		}

		return null;
	}

	public static void loesche(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).getIsbn().equals(isbn)) {
				buchliste.remove(i);
			}
		}

	}

	public static String groesste(List<Buch> buchliste) {
		String max = buchliste.get(0).getIsbn();
		for (int i = 1; i < buchliste.size(); i++) {
			if (buchliste.get(i).getIsbn().compareTo(max) > 0) {
				max = buchliste.get(i).getIsbn();
			}
		}
		return max;

	}
	public static String zeige(List<Buch> buchliste) {
		String f = "";
		for(int i = 0;i <buchliste.size();i++) {
			f= buchliste.toString();
			
		}
    		
		
	return f;  
	}
}

// public static String ermitteleGroessteISBN(........) {
// add your implementation
// }
