package main;

import java.util.Random;

public class Interpolationssuche {

	private final int NICHT_GEFUNDEN = -1;
	long[] zahlenliste = new long[20000000];

	public Interpolationssuche() {
	}

	public long[] getSortedList() {
		Random rand = new Random(111111);

		long naechsteZahl = 0;

		for (int i = 0; i < zahlenliste.length; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}

	public long suche(int von, int bis, long gesuchteZahl) {
		if (von < bis) {
			int bereich = (int) (zahlenliste[bis] - zahlenliste[von]);
			int grenze = (int) (von + (bis - von) * ((gesuchteZahl - zahlenliste[von]) / bereich));
			if (zahlenliste.length == 0) {
				return NICHT_GEFUNDEN;
			}

			if (grenze >= zahlenliste.length) {
				return NICHT_GEFUNDEN;
			}
			if (gesuchteZahl > zahlenliste[grenze]) {
				suche(grenze + 1, bis, gesuchteZahl);
			} else if (gesuchteZahl < zahlenliste[grenze] && von != grenze) {
				suche(von, grenze - 1, gesuchteZahl);
			} else if (gesuchteZahl == zahlenliste[grenze]) {
				return grenze;
			} else {
				return NICHT_GEFUNDEN;
			}
		}
		return NICHT_GEFUNDEN;
	}
}
