
public class Spieler extends mitglieder {

	private int nummer;
	private String position;
	
	public Spieler(String name, String telefonnummer, boolean isJahresbeitrag, int nummer, String position) {
		super( name,  telefonnummer, isJahresbeitrag);
		this.nummer = nummer;
		this.position = position;
	}
	public int getNummer() {
		return nummer;
	}
	public void setNummer(int nummer) {
		this.nummer = nummer;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	@Override
	public String toString() {
		return "name=" +name+"       telefonnummer="+telefonnummer+"       isJahresbeitrag="+isJahresbeitrag+"		Spieler [nummer=" + nummer + ", position=" + position + "]";
	}

}
