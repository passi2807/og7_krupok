
public class Mannschaftsleiter extends Spieler {
	private boolean engagement;

	public Mannschaftsleiter(String name, String telefonnummer, boolean isJahresbeitrag,int nummer,String position,boolean engagement) {
		super(name, telefonnummer, isJahresbeitrag, nummer, position);
		this.engagement = engagement;
		
	}

	public boolean isEngagement() {
		return engagement;
	}

	public void setEngagement(boolean engagement) {
		this.engagement = engagement;
	}

	@Override
	public String toString() {
		return "name=" +name+"       telefonnummer="+telefonnummer+"       isJahresbeitrag="+isJahresbeitrag+"		Mannschaftsleiter [engagement=" + engagement + "]";
	}

}
