
public class mitglieder {

	protected String name;
	protected String telefonnummer;
	protected boolean isJahresbeitrag;
	protected boolean spielerNummer;
	
	public mitglieder() {}
	public mitglieder(String name, String telefonnummer, boolean isJahresbeitrag) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.isJahresbeitrag = isJahresbeitrag;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitrag() {
		return isJahresbeitrag;
	}
	public void setJahresbeitrag(boolean isJahresbeitrag) {
		this.isJahresbeitrag = isJahresbeitrag;
	}
	public boolean isSpielerNummer() {
		return spielerNummer;
	}
	public void setSpielerNummer(boolean spielerNummer) {
		this.spielerNummer = spielerNummer;
	}
	@Override
	public String toString() {
		return "mitglieder [name=" + name + ", telefonnummer=" + telefonnummer + ", isJahresbeitrag=" + isJahresbeitrag
				+ ", spielerNummer=" + spielerNummer + "]";
	}
	
	
}
