
public class Trainer extends mitglieder {
	
	private char lizenzklasse;
	private int aufwandsEntschaedigung;
	
	public Trainer(String name,String telefonnummer,boolean isJahresbeitrag,char lizenzklasse, int aufwandsEntschaedigung) {
		super(name,telefonnummer,isJahresbeitrag);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsEntschaedigung = aufwandsEntschaedigung;
		
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandsEntsch�digung() {
		return aufwandsEntschaedigung;
	}

	public void setAufwandsEntschaedigung(int aufwandsEntschaedigung) {
		this.aufwandsEntschaedigung = aufwandsEntschaedigung;
	}

	@Override
	public String toString() {
		return "name=" +name+"       telefonnummer="+telefonnummer+"       isJahresbeitrag="+isJahresbeitrag+"       Trainer [lizenzklasse=" + lizenzklasse + ", aufwandsEntschaedigung=" + aufwandsEntschaedigung + "]";
	}
	
	


}
