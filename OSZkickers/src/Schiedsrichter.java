
public class Schiedsrichter extends mitglieder {
	private int anzahlSpiele;
	

	public Schiedsrichter(String name,String telefonnummer,boolean isJahresbeitrag,int anzahlSpiele) {
		super(name,telefonnummer,isJahresbeitrag);
		this.anzahlSpiele = anzahlSpiele;
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}

	@Override
	public String toString() {
		return "name=" +name+"       telefonnummer="+telefonnummer+"       isJahresbeitrag="+isJahresbeitrag+"		Schiedsrichter [anzahlSpiele=" + anzahlSpiele + "]";
	}

}
