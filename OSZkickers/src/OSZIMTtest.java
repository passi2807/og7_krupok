
public class OSZIMTtest {
	
	public static void main(String[] args) {
		Trainer t1 = new Trainer("Hans","675675",true,'a',450);
		Schiedsrichter s1 = new Schiedsrichter("Max","1234567814",true,4);
		Mannschaftsleiter m1 = new Mannschaftsleiter("Eric","14343244",false,7,"angriff",true);
		Spieler a1 = new Spieler("Jeldrik","23123123",true,23,"abwehr");

		System.out.println(t1.toString());
		System.out.println(s1.toString());
		System.out.println(m1.toString());
		System.out.println(a1.toString());
	}

}
