
public class StoppuhrNano {
  //Attribute
  private long start;
  private long stopp;

  //Methoden
  public void start(){
    this.start = System.nanoTime();
  } 
  
  public void stopp(){
    this.stopp = System.nanoTime();
  }
  
  public void reset(){
    this.start = 0;
    this.stopp = 0;
  }
  
  public long getNanos(){
    return Math.abs(start - stopp);
  }
  
  public long getMillis() {
	  return Math.round(getNanos() /1000000.0);
  }
  
  public long getSeconds(){
    return Math.round((start - stopp) /1000000000.0);
  }
}

